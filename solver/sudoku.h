#ifndef SUDOKU_H
#define SUDOKU_H

#include <vector>

#include "cell.h"

class Sudoku
{
    public:
        Sudoku();
        Sudoku(std::vector<std::vector<int>> puzzle);

        void SetSudoku(std::vector<std::vector<int>> puzzle);
        
        void SetCell(int r, int c, int digit);

        void SetCell(Cell cell);

        std::vector<int> GetRow(int r);

        std::vector<int> GetCol(int c);

        std::vector<int> GetBox(int r, int c);

        void print_sudoku();

        int solve(std::vector<Cell> cells);

        std::vector<std::vector<int>> GetPuzzle() { return puzzle_; }

        bool CheckSudokuRules(Cell cell);

        bool CheckArea(std::vector<int> area);
    private:
        std::vector<std::vector<int>> puzzle_;

        std::vector<std::vector<Cell>> empties_;
};

#endif

