#include <iostream>
#include <set>
#include <string>

#include "cell.h"
#include "sudoku.h"

#define ROW 9
#define COL 9

//Sudoku Empty Constructor
Sudoku::Sudoku()
{
    for (int i = 0; i < ROW; i++)
    {
        std::vector<int> v1;
        for (int j = 0; j < COL; j++)
        {
            v1.push_back(0);
        }
        puzzle_.push_back(v1);
    }
}

// Sudoku Constructor
Sudoku::Sudoku(std::vector<std::vector<int>> puzzle)
{
    SetSudoku(puzzle);
}

void Sudoku::SetSudoku(std::vector<std::vector<int>> puzzle)
{
    puzzle_ = puzzle;
}

void Sudoku::SetCell(int r, int c, int digit)
{
    if (r < 1 || r > 9)
    {
        std::cout << "Row Out of Bounds" << std::endl;
        return;
    }
    if (c < 1 || c > 9)
    {
        std::cout << "Col Out of Bounds" << std::endl;
        return;
    }

    r = r - 1;
    c = c - 1;
    puzzle_[r][c] = digit;
}

void Sudoku::SetCell(Cell cell)
{
    int r = cell.GetRowCoord() + 1;
    int c = cell.GetColCoord() + 1;
    int value = cell.GetValue();
    SetCell(r, c, value);
}

std::vector<int> Sudoku::GetRow(int r)
{
    if (r < 1 || r > 9)
    {
        std::cout << "Row Out of Bounds" << std::endl;
        std::vector<int> empty;
        return empty;
    }

    r = r - 1;

    return puzzle_[r];
}

std::vector<int> Sudoku::GetCol(int c)
{
    if (c < 1 || c > 9)
    {
        std::cout << "Col Out of Bounds" << std::endl;
        std::vector<int> empty;
        return empty;
    }
    c = c - 1;

    std::vector<int> vec;

    for (int j = 0; j < COL; j++)
    {
        vec.push_back(puzzle_[j][c]);
    }
    return vec;
}

std::vector<int> Sudoku::GetBox(int r, int c)
{
    if (r < 1 || r > 9)
    {
        std::cout << "Row Out of Bounds" << std::endl;
        std::vector<int> empty;
        return empty;
    }
    if (c < 1 || c > 9)
    {
        std::cout << "Col Out of Bounds" << std::endl;
        std::vector<int> empty;
        return empty;
    }

    r = r - 1;
    c = c - 1;

    //
    int rem_r = r % 3;
    int start_r = r - rem_r;

    int rem_c = c % 3;
    int start_c = c - rem_c;

    std::vector<int> box_cells;

    for (int i = start_r; i < start_r + 3; i++)
    {
        for (int j = start_c; j < start_c + 3; j++)
        {
            box_cells.push_back(puzzle_[i][j]);
        }
    }
    return box_cells;

}

bool Sudoku::CheckArea(std::vector<int> area)
{
    std::vector<int> nonzeros;
    for (int i = 0; i < area.size(); i++)
    {
        if (area[i] > 0)
        {
            nonzeros.push_back(area[i]);
        }
    }
    
    std::set<int> s(nonzeros.begin(), nonzeros.end());
    return s.size() == nonzeros.size();
}


bool Sudoku::CheckSudokuRules(Cell cell)
{
    int value = cell.GetValue();

    if (value == 0)
    {
        return false;
    }
    
    int r = cell.GetRowCoord() + 1;
    int c = cell.GetColCoord() + 1;

    std::vector<int> row = GetRow(r);
    std::vector<int> col = GetCol(c);
    std::vector<int> box = GetBox(r, c);

    row.push_back(value);
    col.push_back(value);
    box.push_back(value);

    // for (int i = 0; i < row.size(); i++)
    // {
    //     std::cout << row[i] << " ";
    // }

    // std::cout << std::endl;

    // for (int i = 0; i < col.size(); i++)
    // {
    //     std::cout << col[i] << " ";
    // }

    // std::cout << std::endl;

    // for (int i = 0; i < box.size(); i++)
    // {
    //     std::cout << box[i] << " ";
    // }

    // std::cout << std::endl;

    if (CheckArea(row) & CheckArea(col) & CheckArea(box))
    {
        return true;
    }
    else
    {
        return false;
    }
    

}

void CellStatus(Cell cell)
{
    std::cout << "Cell: (" << cell.GetRowCoord()+1 << ", " << cell.GetColCoord()+1 << ") with Value: " << cell.GetValue() << std::endl;
}

int Sudoku::solve(std::vector<Cell> cells)
{
    // puzzle1 = puzzle_;
    bool backtrack = false;
    int i = 0;
    int p = 0;

    while (true)
    {
        while (backtrack == false)
        {
            // incrementer = cells[i];
            cells[i].Increment();
            std::cout << "Initial Increment: ";
            CellStatus(cells[i]);
            while (this->CheckSudokuRules(cells[i]) == false)
            {
                if (cells[i].Increment() == false)
                {
                    std::cout << "Invalid Increment on: ";
                    CellStatus(cells[i]);
                    backtrack = true;
                    break;
                }
            }
            if (backtrack == false)
            {
                std::cout << "Fits Board: ";
                CellStatus(cells[i]);
                this->SetCell(cells[i]);
                this->print_sudoku();
                std::cout << std::endl;
                i++;
                if (i == cells.size())
                {
                    std::cout << "Done" << std::endl;
                    return 0;
                }
            }
        }

        std::cout << "Backtracking on: ";
        CellStatus(cells[i]);

        while (backtrack == true)
        {
            // Take a Look Back
            // i--;
            p = i - 1;
            if (p < 0)
            {
                std::cout << "Out of Cells" << std::endl;
                return 0;
            }
            std::cout << "Peeking at Previous: ";
            CellStatus(cells[p]);

            if (cells[p].Increment() == true)
            {
                // Incrementing Possible
                // Decrement to Restore peek's state
                cells[p].Decrement();
                
                // Finish Resetting the ith cell onto the board
                cells[i].ResetValue();
                std::cout << "Resetting Current: ";
                CellStatus(cells[i]);

                this->SetCell(cells[i]);                
                // Move i back to peek's

                i = p;
                backtrack = false;
                this->print_sudoku();
                break;
            } else {
                // Incrementing Previous Failed
                std::cout << "Cant Increment on: ";
                CellStatus(cells[p]);

                // Definitely reset the ith cell onto the board
                cells[i].ResetValue();
                std::cout << "Resetting Current: ";
                CellStatus(cells[i]);
                this->SetCell(cells[i]);

                // Peek back is now i
                i = p;
                this->print_sudoku();
            }
        }
    }
    return 0;
}

void Sudoku::print_sudoku()
{
    std::cout << "Grid:" << std::endl;
    for (int i = 0; i < puzzle_.size(); i++)
    {
        for (int j = 0; j < puzzle_[i].size(); j++)
        {
            std::cout << puzzle_[i][j] << " ";
            if (j == 2 || j == 5)
            {
                std::cout << '|' << " ";
            }
        }
        if (i == 2 || i == 5) 
        {
            std::string horiz_line(21, '-');
            std::cout << std::endl << horiz_line;
        }
        std::cout << std::endl;
    }
}
