#include <iostream>

#include "cell.h"

Cell::Cell(int r, int c)
{
    solved = false;
    value = 0;
    possibilities = {1, 2, 3, 4, 5, 6, 7, 8, 9};
    
    row = r;
    col = c;

    current_index = 0;
}

bool Cell::SetValue(int v)
{
    if (v < 1 || v > 9)
    {
        std::cout << "Value " << v << " Out of Bounds" << std::endl;
        std::vector<int> empty;
        return false;
    }
    value = v;
    return true;
}

void Cell::ResetValue()
{
    value = 0;
    current_index = 0;
    return;
}

bool Cell::Increment()
{
    current_index++;
    if (SetValue(current_index) == false)
    {
        return false;
    }
    return true;
}

bool Cell::Decrement()
{
    current_index--;
    if (SetValue(current_index) == false)
    {
        return false;
    }
    return true;
}