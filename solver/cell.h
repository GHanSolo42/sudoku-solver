#ifndef CELL_H
#define CELL_H

#include <vector>

class Cell
// The Memory State of each Cell
{
    public:
        Cell(int r, int c);

        bool SetValue(int v);

        void ResetValue();

        int GetValue() {return value;}

        int GetRowCoord() {return row;}

        int GetColCoord() {return col;}

        bool Increment();

        bool Decrement();

    private:
        bool solved;
        int value;
        std::vector<int> possibilities;
        
        int row;
        int col;

        int current_index;
};

#endif