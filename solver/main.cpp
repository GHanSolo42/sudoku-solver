#include <iostream>

#include "sudoku.h"

#define ROW 9
#define COL 9


int main()
{
    Sudoku puzzle1;
    // puzzle1.SetCell(3,3,7);

    // puzzle1.SetCell(2,4,2);
    // puzzle1.SetCell(3,5,1);
    // puzzle1.SetCell(2,6,7);
    // puzzle1.SetCell(3,6,3);

    // puzzle1.SetCell(2,7,8);
    // puzzle1.SetCell(2,9,1);

    // puzzle1.SetCell(4,2,9);
    // puzzle1.SetCell(6,2,5);

    // puzzle1.SetCell(5,4,7);
    // puzzle1.SetCell(6,4,1);
    // puzzle1.SetCell(5,5,4);
    // puzzle1.SetCell(4,6,2);

    // puzzle1.SetCell(6,7,2);
    // puzzle1.SetCell(4,8,5);
    // puzzle1.SetCell(4,9,7);
    // puzzle1.SetCell(5,9,6);

    // puzzle1.SetCell(8,1,1);
    // puzzle1.SetCell(8,2,8);

    // puzzle1.SetCell(8,4,4);
    // puzzle1.SetCell(8,5,2);
    // puzzle1.SetCell(9,5,6);

    // puzzle1.SetCell(9,7,9);
    // puzzle1.SetCell(8,8,6);
    // puzzle1.SetCell(7,9,5);

    // std::vector<std::vector<int>>
    //     vec{
    //         {8, 7, 1, 0, 0, 0, 3, 4, 0},
    //         {3, 0, 0, 5, 1, 0, 0, 0, 6},
    //         {0, 5, 0, 7, 0, 0, 0, 9, 1},
    //         {0, 3, 0, 0, 0, 6, 0, 0, 9},
    //         {0, 9, 8, 3, 4, 0, 0, 0, 2},
    //         {7, 0, 4, 0, 0, 0, 5, 0, 8},
    //         {0, 2, 7, 0, 8, 0, 9, 0, 0},
    //         {9, 0, 5, 4, 0, 0, 6, 0, 0},
    //         {6, 8, 3, 0, 0, 1, 2, 0, 0}
    //     };
    // Sudoku puzzle1(vec);

    std::cout << "Starting Grid" << std::endl;
    puzzle1.print_sudoku();
    std::cout << std::endl;

    std::vector<std::vector<int>> puzzle = puzzle1.GetPuzzle();

    std::vector<Cell> cells;

    for (int i = 0; i < ROW; i++)
    {
        for (int j = 0; j < COL; j++)
        {
            if (puzzle[i][j] == 0)
            {
                // std::cout << i << " " << j << std::endl;
                Cell cell(i, j);
                cells.push_back(cell);
            }
        }
    }

    puzzle1.solve(cells);

    return 0;
}
